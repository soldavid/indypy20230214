# IndyPy 2023-02-14

## Using AWS Artificial Intelligence services with Python

![Talk Hero](images/talkhero.png)

The notebook for the talk is [cloud.ipynb](cloud.ipynb).

## Requirements

- Python 3.8+
- Poetry
- An Access and Secret key for an AWS account with permissions to:
  - Comprehend
  - Polly
  - Rekognition
  - Transcribe
  - Translate

## Instructions

Run in the console

```bash
poetry install
poetry shell
jupyter notebook cloud.ipynb
```

## Thank you very much
